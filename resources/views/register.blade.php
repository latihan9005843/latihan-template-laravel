@extends('master')

@section('judul')
HALAMAN FORM
@endsection

@section('content')
    <h1>BUAT ACCOUNT BARU</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="post">
        @csrf
        <label>First Name :</label><br>
        <input type="text" name="firstname"><br><br>
        <label>Last Name :</label><br>
        <input type="text" name="lastname"><br><br>
        <label>Gender</label><br>
        <input type="radio"name="wn"> Male <br> 
        <input type="radio"name="wn">Female<br><br>
        <label>Nationality</label><br><br>
        <select name="Nationality">
            <option value="indonesia">Indonesia</option>
            <option value="arab">Arab</option>
            <option value="england">England</option>
            <option value="chinese">Chinese</option>
            <option value="japan">Japan</option>
        </select><br><br>
        <label>Languange Spoken</label><br>
        <input type="checkbox" name="languange">Indonesia<br>
        <input type="checkbox" name="languange">English<br>
        <input type="checkbox" name="languange">Other<br><br>
        <label>Bio</label><br>
        <textarea name="Bio" rows="10" cols="30"></textarea>
        <br><br>
        <input type="submit" value="Sign Up">



    </form>

@endsection
    