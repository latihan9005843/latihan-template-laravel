@extends('master')

@section('judul')
HALAMAN HOME

@endsection

@section('content')
    <h1>MEDIA ONLINE</h1>
    <h3>Sosial Media Developer</h3>
    <p>Belajar dan berbagi agar hidup lebih baik</p>
 
    <h3>Benefit Join di Media Online</h3>  
    <ul>
        <li>Mendapatkan motivasi dari sesama developer</li>
        <li>Sharing Knowledge</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
 
    <h3>Cara Bergabung ke Media Online</h3>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="/register">FORM SIGN UP</a></li>
        <li>Selesai</li>
    </ol>
@endsection



