@extends('master')

@section('judul')
Halaman Show
@endsection

@section('content')

<h2>Cast {{$cast->id}}</h2>
<div class="card">
    <div class="card-body">
        <label>Nama</label>
        <h4>{{$cast->nama }}</h4>
        <label>Umur :</label>
        <p>{{$cast->umur }}</p>
        <label>Biografi :</label>
        <p>{{$cast->bio }}</p>

    </div>
</div>

@endsection