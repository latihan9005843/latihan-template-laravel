@extends('master')

@section('judul')
Halaman Tambah cast
@endsection

@section('content')

<form action="{{  route('caster.store')  }}" method="POST">
    @csrf
    <div class="form-group">
        <label>Nama</label>
        <input type="text" name="nama" class="form-control" id="nama" value="{{ old('nama')}}" placeholder="Masukkan Nama">
    
    @error('nama')
        <div class="alert alert danger">{{$message}}</div>
    @enderror
    </div>
    <div class="form-group">
        <label>Umur</label>
        <input type="number" name="umur" class="form-control" id="umur" value="{{ old('umur')}}" placeholder="Masukkan Umur">
    
    @error('umur')
        <div class="alert alert danger">{{$message}}</div>
    @enderror
    </div>
    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" cols="20" rows="4" id="bio">{{ old('bio')}}</textarea>

    @error('bio')
        <div class="alert alert danger">{{$message}}</div>
    @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>



@endsection