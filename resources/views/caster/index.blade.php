@extends('master')

@section('judul')
Halaman Cast
@endsection

@section('content')
<a href="{{ route('caster.create') }}" class="btn btn-primary mb-3">Tambah</a>
        <table class="table table-bordered">
            <thead class="thead-light ">
              <tr>
                <th scope="col" class="text-center">id</th>
                <th scope="col" class="text-center">Nama</th>
                <th scope="col" class="text-center">Umur</th>
                <th scope="col" class="text-center" style="width: 50%">Bio</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key=>$caster)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$caster->nama}}</td>
                        <td>{{$caster->umur}}</td>
                        <td>{{$caster->bio}}</td>
                        <td>
                            <a href="/caster/{{$caster->id}}/show" class="btn btn-info">Show</a>
                            <a href="/caster/{{$caster->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/caster/{{$caster->id}}/destroy" method="POST" class="d-inline">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr >
                        <td colspan="5" class="text-center">No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>


@endsection