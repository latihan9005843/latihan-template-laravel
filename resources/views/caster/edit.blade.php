@extends('master')

@section('judul')
Halaman Edit
@endsection

@section('content')
<h2>Edit {{$cast->id}}</h2>

<form action="/caster/{{ $cast->id }}/update" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama</label>
        <input type="text" name="nama" class="form-control" id="nama" value="{{ old('nama') ?? $cast->nama }}" placeholder="Masukkan Nama">
    
    @error('nama')
        <div class="alert alert danger">{{$message}}</div>
    @enderror
    </div>
    <div class="form-group">
        <label>Umur</label>
        <input type="number" name="umur" class="form-control" id="umur" value="{{ old('umur') ?? $cast->umur }}" placeholder="Masukkan Umur">
    
    @error('umur')
        <div class="alert alert danger">{{$message}}</div>
    @enderror
    </div>
    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" cols="20" rows="4" id="bio">{{ old('bio') ?? $cast->bio }}</textarea>

    @error('bio')
        <div class="alert alert danger">{{$message}}</div>
    @enderror
    </div>
    <div class="text-right">
    <button type="submit" class="btn btn-success">Update</button>
    </div>
</form>



@endsection