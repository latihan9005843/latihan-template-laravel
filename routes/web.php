<?php

use App\Http\Controllers\IndexController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'utama']);

Route::get('/register', [AuthController::class, 'registrasi'] )-> name('register');

Route::post('/welcome', [AuthController::class, 'welcome'] )-> name('register');

Route::get('/data-table', [IndexController::class, 'table']);

//crud
Route::get('/caster', [CastController::class, 'index'])->name('caster.index');

Route::get('/caster/create', [CastController::class, 'create'])->name('caster.create');

Route::post('/caster/store', [CastController::class, 'store'])->name('caster.store');

Route::get('/caster/{id}/show', [CastController::class, 'show'])->name('caster.show');

Route::get('/caster/{id}/edit', [CastController::class, 'edit'])->name('caster.edit');

Route::put('/caster/{id}/update', [CastController::class, 'update'])->name('caster.update');

Route::delete('/caster/{id}/destroy', [CastController::class, 'destroy'])->name('caster.destroy');

